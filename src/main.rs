use lettre::*;
use rand::seq::SliceRandom; // for shuffle
use rand::thread_rng;
use serde::Deserialize;
use std::error::Error;

#[derive(Deserialize, Debug)]
struct Matchee {
    name: String,
    mail: EmailAddress,
    address: String,
    extra: Option<String>,
}

#[derive(Deserialize, Debug)]
struct SantaInput {
    matchees: Vec<Matchee>,
}

fn main() -> Result<(), Box<dyn Error>> {
    let mut matchees: Vec<_> =
        toml::from_str::<SantaInput>(&std::fs::read_to_string("test_matchees.toml")?)?.matchees;
    matchees.shuffle(&mut thread_rng());
    let n = matchees.len();
    let mut matchees_it = matchees.iter().cycle().peekable();
    for _ in 0..n {
        let gifter = &matchees_it.next().unwrap().mail;
        let giftee = &matchees_it.peek().unwrap();
        send_match(gifter, giftee)?;
    }
    Ok(())
}

fn send_match(to: &EmailAddress, m: &Matchee) -> Result<(), Box<dyn Error>> {
    send_mail(
        to,
        "Your ROOT Secret Santa match is here!",
        &fmt_mail_body(m),
    )
}

fn fmt_mail_body(m: &Matchee) -> String {
    let s = format!(
        "Your matched person is {}!\n\nHere is the address they left for you:\n\n\t{}",
        m.name,
        m.address.trim().replace("\n", "\n\t")
    );
    let extra = match &m.extra {
        Some(e) => format!("\n\nExtra info:\n\n\t{}", e.trim().replace("\n", "\n\t")),
        None => String::from("\n\nNo extra info was provided."),
    };
    let footer = "\n\nPlease try to have the gift arrive by the 7th of January, when we'll have an unwrapping event (and don't open yours before then!).\n\nHappy holidays,\nyour friendly neighbourhood Secret Santa bot";
    s + &extra + footer
}

fn send_mail(to: &EmailAddress, subject: &str, body: &str) -> Result<(), Box<dyn Error>> {
    let from = EmailAddress::new("root.secretsanta@cern.ch".to_string())?;
    let envelope = Envelope::new(Some(from), vec![to.clone()])?;
    let mut message: Vec<u8> = format!("Subject: {}\n", subject).as_bytes().to_vec();
    message.append(format!("To: {}\n\n", &to).as_bytes().to_vec().as_mut());
    message.append(body.as_bytes().to_vec().as_mut());
    let mail = SendableEmail::new(envelope, "someid".to_string(), message);
    SendmailTransport::new().send(mail)?;
    Ok(())
}
